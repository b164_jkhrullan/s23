//3

{
    "_id" : ObjectId("6239c239b190789501ae1e84"),
    "firstName" : "single",
    "accomodate" : 2.0,
    "price" : 1000.0,
    "description" : "A simple room with all the basic necessities",
    "rooms_available" : 10.0,
    "isAvailable" : false
}

//4

{
    "_id" : ObjectId("6239c635b190789501ae1e85"),
    "name" : "double",
    "accomodates" : 3.0,
    "price" : 2000.0,
    "description" : "A room fit for a small family going on a vacation",
    "rooms_availble" : 5.0,
    "isAvailable" : false
}

{
    "_id" : ObjectId("6239c635b190789501ae1e86"),
    "name" : "queen",
    "accomodates" : 4.0,
    "price" : 4000.0,
    "description" : "A room with a queen sized bed perfect for a simple getaway",
    "rooms_availble" : 15.0,
    "isAvailable" : false
}


//5

db.user.find({
    name:"double"
    })


//6

db.users.updateOne(
    { name: "queen"},
    {
        $set: {
            
            rooms_available: 0
  
            
            }
        

        
        }

    
    )


//7

db.users.deleteMany(
    {
        rooms_available: 0
    })